FROM node:20 as development
WORKDIR /app
COPY package.json yarn.lock ./
RUN yarn install
COPY ./ ./
EXPOSE 3000
CMD ["npm", "run", "start:dev"]
FROM development as builder
WORKDIR /app
RUN rm -rf node_modules
RUN yarn install --production
EXPOSE 3000
CMD ["npm", "start"]
FROM node:20-slim
WORKDIR /app
COPY --from=builder /app ./
CMD ["node", "src/index.js"]
